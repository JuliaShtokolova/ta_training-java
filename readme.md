You can run smoke test using this commands in the terminal built in the IDE:

```shell
 mvn -Denvironment=firefox -Dsurefire.suiteXmlFiles=src/test/resources/testng-smoke.xml clean test
```

or

```shell
mvn -Denvironment=chrome -Dsurefire.suiteXmlFiles=src/test/resources/testng-smoke.xml clean test
```

You can run all tests using this command in the terminal built in the IDE:

```shell
 mvn -Denvironment=firefox -Dsurefire.suiteXmlFiles=src/test/resources/testng-all.xml clean test
```