package com.epam.training.student_julia_shtokolova;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class NewPasteBash {
    private static final String BASH_CODE = "git config --global user.name  \"New Sheriff in Town\"\n" +
            "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
            "git push origin master --force\n";

    @Test
    public void newPasteBash() {
        WebDriver driver = new FirefoxDriver();
        driver.get("https://pastebin.com/");
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(20));

        // Waiting for the banner
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("vi-smartbanner")));

        // Programmatically close the banner
        ((JavascriptExecutor) driver).executeScript("document.getElementById('vi-smartbanner').remove()");

        // Filling out the text area for "New Paste"
        WebElement textArea = driver.findElement(By.xpath("//*[@id=\"postform-text\"]"));
        textArea.sendKeys(BASH_CODE);

        // Setting "Syntax Highlighting"
        WebElement dropDownSyntax = driver.findElement(By.id("select2-postform-format-container"));
        dropDownSyntax.click();
        WebElement dropDownSyntaxOption = driver.findElement(By.xpath(
                "//*[@id='select2-postform-format-results']//li[@role='option' and text()='Bash']"
        ));
        dropDownSyntaxOption.click();

        // Setting "Paste expiration"
        WebElement select = driver.findElement(By.id("select2-postform-expiration-container"));
        select.click();
        WebElement option = driver.findElement(By.xpath(
                "//*[@id='select2-postform-expiration-results']//li[@role='option' and text()='10 Minutes']"
        ));
        option.click();

        // Setting "Paste Name/Title"
        WebElement pasteTitle = driver.findElement(By.id("postform-name"));
        String pasteTitleText = "how to gain dominance among developers";
        pasteTitle.sendKeys(pasteTitleText);

        // Submitting new paste
        WebElement createNewPasteBtn = driver.findElement(By.cssSelector(
                ".content .post-create form button[type=submit]"));
        createNewPasteBtn.click();

        // Waiting until page reloads
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1")));

        // Checking if browser page title matches "Paste Name / Title"
        String actualTitle = driver.findElement(By.xpath("//h1")).getText();
        Assertions.assertEquals(pasteTitleText, actualTitle);

        // Checking if the syntax matches the syntax selected when creating new paste
        String actualSyntax = driver
                .findElement(By.xpath("//div[@class='top-buttons']//div[@class='left']//a[contains(@class, 'btn')][1]"))
                .getText();
        Assertions.assertEquals("Bash", actualSyntax);

        // Checking if the code matches the code entered when creating new paste
        String actualCode = driver
                .findElement(By.xpath("//ol[@class='bash']"))
                .getText()
                .trim();
        Assertions.assertEquals(BASH_CODE.trim(), actualCode);

        driver.quit();
    }
}
