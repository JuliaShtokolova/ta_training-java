package com.epam.training.student_julia_shtokolova.test;

import com.epam.training.student_julia_shtokolova.page.google_cloud.GoogleCloudHomePage;
import com.epam.training.student_julia_shtokolova.page.BaseComponent;
import com.epam.training.student_julia_shtokolova.page.google_cloud.search.components.CalcLegacyLinkComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class SearchFunctionalityTest extends CommonConditions{
    @Test
    public void canPerformSearch() {
        Logger logger = LogManager.getLogger(SearchFunctionalityTest.class);

        setUp();
        var results = new GoogleCloudHomePage(driver)
                .open()
                .search("Google Cloud Platform Pricing Calculator")
                .getResults();

        CalcLegacyLinkComponent calcLink = null;

        for (BaseComponent link: results) {
            if (link instanceof CalcLegacyLinkComponent) {
                calcLink = (CalcLegacyLinkComponent) link;
            }
        }

        assertThat(calcLink, is(notNullValue()));
        logger.info("Search has been performed");
    }
}
