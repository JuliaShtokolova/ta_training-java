package com.epam.training.student_julia_shtokolova.page.google_cloud;

import com.epam.training.student_julia_shtokolova.page.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleCloudHomePage extends BasePage {
    private static final String PAGE_URL = "https://cloud.google.com/";

    @FindBy(xpath = "//header/div[2]/div[1]/div/div[2]/div[2]/div[1]/div/div")
    private WebElement searchIcon;

//    @FindBy(xpath = "//header//form//input[@aria-label='Search' and @type='text']")
    @FindBy(xpath = "//input[@placeholder='Search']")
    private WebElement searchField;

    public GoogleCloudHomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @Override
    public String getUrl() {
        return PAGE_URL;
    }

    @Override
    public GoogleCloudHomePage open() {
        super.open();

        return this;
    }

    public GoogleCloudSearchPage search(String text) {
        searchIcon.click();
        searchField.sendKeys(text + Keys.ENTER);

        return new GoogleCloudSearchPage(driver);
    }
}
