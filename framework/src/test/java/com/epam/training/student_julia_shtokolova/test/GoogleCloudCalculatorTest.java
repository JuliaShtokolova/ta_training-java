package com.epam.training.student_julia_shtokolova.test;

import com.epam.training.student_julia_shtokolova.page.google_cloud.GoogleCloudCalcPage;
import com.epam.training.student_julia_shtokolova.page.google_cloud.GoogleCloudHomePage;
import com.epam.training.student_julia_shtokolova.page.google_cloud.calc.enums.*;
import com.epam.training.student_julia_shtokolova.page.yopmail.YopmailHomePage;
import com.epam.training.student_julia_shtokolova.page.BaseComponent;
import com.epam.training.student_julia_shtokolova.page.google_cloud.search.components.CalcLegacyLinkComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WindowType;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GoogleCloudCalculatorTest extends CommonConditions {

    @Test
    public void canGetEstimationByEmail() {
        Logger logger = LogManager.getLogger(SearchFunctionalityTest.class);
        
        setUp();

        var results = new GoogleCloudHomePage(driver)
            .open()
            .search("Google Cloud Platform Pricing Calculator")
            .getResults();

        CalcLegacyLinkComponent calcLink = null;

        for (BaseComponent link: results) {
            if (link instanceof CalcLegacyLinkComponent) {
                calcLink = (CalcLegacyLinkComponent) link;
            }
        }

        Assert.assertNotNull(calcLink);

        GoogleCloudCalcPage calcPage = calcLink.open();

        // According to the task requirement we should pick "GPU type: NVIDIA Tesla V100",
        // but this option unavailable. We pick GPU type: NVIDIA Tesla T4
        calcPage
            .openComputeEngineTab()
            .setNumberOfInstances(10)
            .setInstancesFor(null)
            .setOperationSystem(OS.FREE)
            .setProvisioningModel(ProvisioningModel.REGULAR)
            .setMachineFamily(MachineFamily.GENERAL)
            .setSeries(Series.N1)
            .setMachineType(MachineType.N1S8)
            .selectAddGpu()
            .setGpuType(GPUType.T4)
            .setNumberOfGpu(NumberOfGpus.N1)
            .setLocalSsd(SSDType.N2)
            .setDatacenterLocation(Region.FRANKFURT)
            .setCommittedUsage(CommittedUsage.YEAR1)
            .clickAddToEstimate();

        Assert.assertTrue(
            calcPage.getTotalResultText().matches("Total Estimated Cost: USD [\\d,\\.]+ per 1 month")
        );

        String tabGoogleCalc = driver.getWindowHandle();
        driver.switchTo().newWindow(WindowType.TAB);
        String tabYopMail = driver.getWindowHandle();

        YopmailHomePage yopmailPage = new YopmailHomePage(driver);
        String email = yopmailPage.getEmail();

        driver.switchTo().window(tabGoogleCalc);

        calcPage
            .openEmailForm()
            .setEmail(email)
            .submit();

        String amountText = calcPage.getTotalAmount();

        driver.switchTo().window(tabYopMail);

        Assert.assertTrue(
            yopmailPage
                .openMailbox()
                .waitForEmailFrom("gcp-estimate@cloudpricingcalculator.appspotmail.com")
                .canFind(amountText)
        );
        logger.info("The estimate was received by mail");
    }
}
