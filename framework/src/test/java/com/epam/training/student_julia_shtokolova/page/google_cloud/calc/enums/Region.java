package com.epam.training.student_julia_shtokolova.page.google_cloud.calc.enums;

public enum Region {
    FRANKFURT("europe-west3");

    private final String value;

    Region(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
