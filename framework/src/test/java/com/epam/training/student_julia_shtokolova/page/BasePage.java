package com.epam.training.student_julia_shtokolova.page;

import org.openqa.selenium.WebDriver;

import java.time.Duration;

public abstract class BasePage {
    protected WebDriver driver;

    public abstract String getUrl();

    protected BasePage() {
        throw new IllegalArgumentException("The driver argument is required");
    }

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public BasePage open() {
        String url = getUrl();

        if (url == null) {
            throw new UnsupportedOperationException();
        }

        driver.get(url);
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(20));

        return this;
    }
}
