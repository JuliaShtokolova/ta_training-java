package com.epam.training.student_julia_shtokolova.driver;

import com.epam.training.student_julia_shtokolova.service.TestDataReader;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

public class DriverSingleton {
    private static WebDriver driver;
    public static final String DRIVER_TO_USE = "web.driver";

    private DriverSingleton(){}

    public static WebDriver getDriver(){
        if (null == driver){
//            if (System.getProperty("browser", "firefox").equals("firefox")) {
//             else {
//                WebDriverManager.chromedriver().setup();
//                driver = new ChromeDriver();
//            }
            if (TestDataReader.getTestData(DRIVER_TO_USE).equals("firefox")) {
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
            }
            if (TestDataReader.getTestData(DRIVER_TO_USE).equals("chrome")) {
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
            }

            driver.manage().window().maximize();
        }

        return driver;
    }

    public static void closeDriver(){
        driver.quit();
        driver = null;
    }
}
