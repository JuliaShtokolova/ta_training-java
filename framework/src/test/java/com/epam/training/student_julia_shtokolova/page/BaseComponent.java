package com.epam.training.student_julia_shtokolova.page;

import org.openqa.selenium.WebElement;

public abstract class BaseComponent {
    protected WebElement root;

    public BaseComponent(WebElement root) {
        this.root = root;

    }
}
