package com.epam.training.student_julia_shtokolova.page.google_cloud;

import com.epam.training.student_julia_shtokolova.page.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class GoogleCloudHomePage extends BasePage {
    private static final String PAGE_URL = "https://cloud.google.com/";
    private final By searchBtn = By.xpath("//header/div[2]/div[1]/div/div[2]/div[2]/div[1]/div/div");
    private final By searchTxt = By.xpath("//header//form//input[@aria-label='Search' and @type='text']");

    public GoogleCloudHomePage(WebDriver driver) {
        super(driver);
    }

    @Override
    public String getUrl() {
        return PAGE_URL;
    }

    @Override
    public GoogleCloudHomePage open() {
        super.open();
        return this;
    }

    public GoogleCloudSearchPage search(String text) {
        driver
            .findElement(searchBtn)
            .click();

        driver
            .findElement(searchTxt)
            .sendKeys(text + Keys.ENTER);

        return new GoogleCloudSearchPage(driver);
    }
}
