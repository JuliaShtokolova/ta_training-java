package com.epam.training.student_julia_shtokolova.page.google_cloud;

import com.epam.training.student_julia_shtokolova.page.BasePage;
import com.epam.training.student_julia_shtokolova.page.google_cloud.calc.components.ComputeEngineTabComponent;
import com.epam.training.student_julia_shtokolova.page.google_cloud.calc.components.EmailFormComponent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GoogleCloudCalcPage extends BasePage {
    public static final String PAGE_URL = "https://cloud.google.com/products/calculator-legacy";
    private final By frame1 = By.cssSelector("#cloud-site iframe");
    private final By frame2 = By.id("myFrame");
    private final By computeEngineTab = By.xpath("//md-tab-item[@id='tab-item-1']");
    private final By computeEngineForm = By.xpath("//md-card-content[@id='mainForm']");
    private final By totalResultTxt = By.xpath("//div[@class='cpc-cart-total']//b[@class='ng-binding']");
    private final By emailEstimateBtn = By.xpath("//*[@id=\"Email Estimate\"]");
    private final By emailEstimateDialog = By.xpath("//md-dialog[@aria-label='Email Estiamte']");

    public GoogleCloudCalcPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public String getUrl() {
        return PAGE_URL;
    }

    @Override
    public GoogleCloudCalcPage open() {
        super.open();
        return this;
    }

    public ComputeEngineTabComponent openComputeEngineTab() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame1));
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame2));

        driver
            .findElement(computeEngineTab)
            .click();

        return new ComputeEngineTabComponent(
                driver.findElement(computeEngineForm)
        );
    }

    private GoogleCloudCalcPage switchToContentFrame() {
        driver.switchTo().frame(0);
        driver.switchTo().frame(0);
        return this;
    }

    public String getTotalResultText() {
        return driver
            .findElement(totalResultTxt)
            .getText();
    }

    public String getTotalAmount() {
        Matcher m = Pattern.compile("USD [\\d,\\.]+").matcher(getTotalResultText());
        return m.find() ? m.group() : null;
    }

    public EmailFormComponent openEmailForm() {
        switchToContentFrame()
            .driver
            .findElement(emailEstimateBtn)
            .click();

        return new EmailFormComponent(
            driver.findElement(emailEstimateDialog)
        );
    }
}
