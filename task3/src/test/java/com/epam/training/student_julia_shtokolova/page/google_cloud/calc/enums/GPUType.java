package com.epam.training.student_julia_shtokolova.page.google_cloud.calc.enums;

public enum GPUType {
    K80,
    P100,
    P4,
    V100,
    T4
}
