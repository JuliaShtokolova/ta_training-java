package com.epam.training.student_julia_shtokolova.page.google_cloud.search.components;

import com.epam.training.student_julia_shtokolova.page.BaseComponent;
import com.epam.training.student_julia_shtokolova.page.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchLinkComponent extends BaseComponent {
    WebDriver driver;

    public SearchLinkComponent(WebElement root, WebDriver driver) {
        super(root);
        this.driver = driver;
    }

    public BasePage open() {
        throw new UnsupportedOperationException();
    }
}
