package com.epam.training.student_julia_shtokolova.page.google_cloud.calc.components;

import com.epam.training.student_julia_shtokolova.page.BaseComponent;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;

public class EmailFormComponent extends BaseComponent {
    public EmailFormComponent(WebElement root) {
        super(root);
    }
    private final By emailAddress = By.xpath("//input[@ng-model='emailQuote.user.email']");
    private final By submitBtn = By.xpath(".//button[contains(text(), 'Send Email')]");

    public EmailFormComponent setEmail(String email) {
        root
            .findElement(emailAddress)
            .sendKeys(email);

        return this;
    }

    public EmailFormComponent submit() {
        root
            .findElement(submitBtn)
            .click();

        return this;
    }
}
