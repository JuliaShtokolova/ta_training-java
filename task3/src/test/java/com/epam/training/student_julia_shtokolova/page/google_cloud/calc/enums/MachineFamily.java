package com.epam.training.student_julia_shtokolova.page.google_cloud.calc.enums;

public enum MachineFamily {
    GENERAL,
    COMPUTE,
    MEMORY,
    ACCELERATOR
}
