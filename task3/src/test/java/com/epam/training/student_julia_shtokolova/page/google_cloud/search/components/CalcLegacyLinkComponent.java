package com.epam.training.student_julia_shtokolova.page.google_cloud.search.components;

import com.epam.training.student_julia_shtokolova.page.google_cloud.GoogleCloudCalcPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CalcLegacyLinkComponent extends SearchLinkComponent {
    public CalcLegacyLinkComponent(WebElement root, WebDriver driver) {
        super(root, driver);
    }

    @Override
    public GoogleCloudCalcPage open() {
        root.findElement(By.tagName("a")).click();
        return new GoogleCloudCalcPage(driver);
    }
}
