package com.epam.training.student_julia_shtokolova.page.google_cloud.calc.enums;

public enum MachineType {
    N1S1,
    N1S2,
    N1S4,
    N1S8

}
