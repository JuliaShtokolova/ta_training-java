package com.epam.training.student_julia_shtokolova.page.google_cloud.calc.enums;

public enum CommittedUsage {
    NONE,
    YEAR1,
    YEAR2
}
