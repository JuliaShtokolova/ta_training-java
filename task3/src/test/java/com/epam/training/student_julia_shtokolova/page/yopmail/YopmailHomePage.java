package com.epam.training.student_julia_shtokolova.page.yopmail;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class YopmailHomePage {
    public static final String PAGE_URL = "https://yopmail.com/ru/email-generator";
    private final By emailAddress = By.id("egen");
    private final By checkEmailBtn = By.xpath("//button[@onclick='egengo();']");
    WebDriver driver;

    public YopmailHomePage(WebDriver driver) {
        this.driver = driver;
        driver.get(PAGE_URL);
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(20));

        ((JavascriptExecutor) driver)
            .executeScript("for (let e of document.getElementsByTagName('ins')) { e.remove(); }");
    }

    public String getEmail() {
        return driver.findElement(emailAddress).getText();
    }

    private YopmailHomePage closeAds() {
        ((JavascriptExecutor) driver)
            .executeScript("document.querySelectorAll('ins').forEach(e => e.remove());");
        return this;
    }

    public YopmailMailboxPage openMailbox() {
        closeAds()
            .driver
            .findElement(checkEmailBtn)
            .click();

        return new YopmailMailboxPage(driver);
    }
}
