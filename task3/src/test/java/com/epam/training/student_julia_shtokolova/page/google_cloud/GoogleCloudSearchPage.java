package com.epam.training.student_julia_shtokolova.page.google_cloud;

import com.epam.training.student_julia_shtokolova.page.BaseComponent;
import com.epam.training.student_julia_shtokolova.page.BasePage;
import com.epam.training.student_julia_shtokolova.page.google_cloud.search.components.CalcLegacyLinkComponent;
import com.epam.training.student_julia_shtokolova.page.google_cloud.search.components.SearchLinkComponent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GoogleCloudSearchPage extends BasePage {
    public static final String PAGE_URL = "https://cloud.google.com/search";
    private final By searchResultLinks = By.xpath("/html/body/c-wiz[2]/div/div/div/div/div/div[3]/c-wiz/div[1]/div");

    public GoogleCloudSearchPage(WebDriver driver) {
        super(driver);

        new WebDriverWait(driver, Duration.ofSeconds(10))
            .until(ExpectedConditions.urlContains(PAGE_URL));
    }

    @Override
    public GoogleCloudSearchPage open() {
        super.open();
        return this;
    }

    @Override
    public String getUrl() {
        return PAGE_URL;
    }

    public List<BaseComponent> getResults() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        List<WebElement> searchResults = driver.findElements(searchResultLinks);

        List<BaseComponent> results = new ArrayList<>();

        for (WebElement elem: searchResults) {
            WebElement a = elem.findElement(By.xpath(".//a"));

            if (a.getAttribute("href").startsWith(GoogleCloudCalcPage.PAGE_URL)) {
                results.add(new CalcLegacyLinkComponent(elem, driver));
            } else {
                results.add(new SearchLinkComponent(elem, driver));
            }
        }

        return results;
    }
}
