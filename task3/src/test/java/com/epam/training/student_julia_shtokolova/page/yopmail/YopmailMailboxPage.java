package com.epam.training.student_julia_shtokolova.page.yopmail;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class YopmailMailboxPage {
    WebDriver driver;
    private final By emailAddress = By.id("egen");
    private final By refreshBtn = By.id("refresh");

    public YopmailMailboxPage(WebDriver driver) {
        this.driver = driver;
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(20));

        // Programmatically close the banner
        ((JavascriptExecutor) driver)
                .executeScript("for (let e of document.getElementsByTagName('ins')) { e.remove(); }");
    }

    public String getEmail() {
        return driver.findElement(emailAddress).getText();
    }

    public YopmailMailboxPage waitForEmailFrom(String sender) {
        By emailXpath = By.xpath("//span[text()='" + sender + "']");

        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(60))
                .pollingEvery(Duration.ofSeconds(1));

        wait.until(driver -> {
            driver.findElement(refreshBtn).click();

            driver.switchTo().frame("ifinbox");
            boolean exists = !driver.findElements(emailXpath).isEmpty();

            driver.switchTo().parentFrame();
            return exists;
        });

        return this;
    }

    public boolean canFind(String text) {
        driver.switchTo().frame("ifmail");
        boolean exists = !driver.findElements(By.xpath("//*[text()='" + text + "']")).isEmpty();
        driver.switchTo().parentFrame();

        return exists;
    }
}
