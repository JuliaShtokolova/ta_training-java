package com.epam.training.student_julia_shtokolova.page.google_cloud.calc.components;

import com.epam.training.student_julia_shtokolova.page.BaseComponent;
import com.epam.training.student_julia_shtokolova.page.google_cloud.calc.enums.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ComputeEngineTabComponent extends BaseComponent {
    private final By numberOfInstances = By.xpath(".//input[@ng-model='listingCtrl.computeServer.quantity']");
    private final By instancesFor = By.xpath(".//input[@ng-model='listingCtrl.computeServer.label']");
    private final By operationSystem = By.xpath(".//md-select[@ng-model='listingCtrl.computeServer.os']");
    private final By provisioningModel = By.xpath("//md-select[@ng-model='listingCtrl.computeServer.class']");
    private final By machineFamily = By.xpath("//md-select[@placeholder='Machine Family']");
    private final By series = By.xpath("//md-select[@placeholder='Series']");
    private final By machineType = By.xpath("//md-select[@placeholder='Instance type']");
    private final By addGPU = By.xpath("//md-checkbox[@ng-model='listingCtrl.computeServer.addGPUs']");
    private final By gpuType = By.xpath("//md-select[@placeholder='GPU type']");
    private final By numberOfGPU = By.xpath("//md-select[@placeholder='Number of GPUs']");
    private final By localSSD = By.xpath("//md-select[@placeholder='Local SSD']");
    private final By datacenterLocation = By.xpath("//md-select[@placeholder='Datacenter location']");
    private final By committedUsage = By.xpath("//md-select[@placeholder='Committed usage']");
    private final By addToEstimateBtn = By.xpath("//button[@ng-click='listingCtrl.addComputeServer(ComputeEngineForm);']");



    public ComputeEngineTabComponent(WebElement root) {
        super(root);
    }

    public ComputeEngineTabComponent setNumberOfInstances(int value) {
        root
            .findElement(numberOfInstances)
            .sendKeys(String.valueOf(value));

        return this;
    }

    public ComputeEngineTabComponent setInstancesFor(String value) {
        WebElement input = root.findElement(instancesFor);

        if (value == null) {
            input.clear();
        } else {
            input.sendKeys(value);
        }

        return this;
    }

    public ComputeEngineTabComponent setOperationSystem(OS value) {
        root
            .findElement(operationSystem)
            .click();

        String index = String.valueOf(value.ordinal() + 1);

        root
            .findElement(By.xpath("//div[contains(@id, 'select_container') and contains(@class, 'md-active')]//md-option[" + index + "]"))
            .click();

        return this;
    }

    public ComputeEngineTabComponent setProvisioningModel(ProvisioningModel value) {
        root
            .findElement(provisioningModel)
            .click();

        String index = String.valueOf(value.ordinal() + 1);

        root
            .findElement(By.xpath("//div[contains(@id, 'select_container') and contains(@class, 'md-active')]//md-select-menu//md-option[" + index + "]"))
            .click();

        return this;
    }

    public ComputeEngineTabComponent setMachineFamily(MachineFamily value) {
        root
            .findElement(machineFamily)
            .click();

        String index = String.valueOf(value.ordinal() + 1);

        root
            .findElement(By.xpath("//div[contains(@id, 'select_container') and contains(@class, 'md-active')]//md-select-menu//md-option[" + index + "]"))
            .click();

        return this;
    }

    public ComputeEngineTabComponent setSeries(Series value) {
        root
            .findElement(series)
            .click();

        String index = String.valueOf(value.ordinal() + 1);

        root
            .findElement(By.xpath("//div[contains(@id, 'select_container') and contains(@class, 'md-active')]//md-select-menu//md-option[" + index + "]"))
            .click();

        return this;
    }

    public ComputeEngineTabComponent setMachineType(MachineType value) {
        root
            .findElement(machineType)
            .click();

        String index = String.valueOf(value.ordinal() + 1);

        root
            .findElement(By.xpath("//div[contains(@id, 'select_container') and contains(@class, 'md-active')]//md-optgroup[@label='standard']//md-option[" + index + "]"))
            .click();

        return this;
    }

    public ComputeEngineTabComponent selectAddGpu() {
        root
            .findElement(addGPU)
            .click();

        return this;
    }

    public ComputeEngineTabComponent setGpuType(GPUType value) {
        root
            .findElement(gpuType)
            .click();

        String index = String.valueOf(value.ordinal() + 1);

        root
            .findElement(By.xpath("//div[contains(@id, 'select_container') and contains(@class, 'md-active')]//md-option[" + index + "]"))
            .click();

        return this;
    }

    public ComputeEngineTabComponent setNumberOfGpu(NumberOfGpus value) {
        root
            .findElement(numberOfGPU)
            .click();

        String index = String.valueOf(value.ordinal() + 1);

        root
            .findElement(By.xpath("//div[contains(@id, 'select_container') and contains(@class, 'md-active')]//md-option[" + index + "]"))
            .click();

        return this;
    }

    public ComputeEngineTabComponent setLocalSsd(SSDType value) {
        root
            .findElement(localSSD)
            .click();

        String index = String.valueOf(value.ordinal() + 1);

        root
            .findElement(By.xpath("//div[contains(@id, 'select_container') and contains(@class, 'md-active')]//md-option[" + index + "]"))
            .click();

        return this;
    }

    public ComputeEngineTabComponent setDatacenterLocation(Region value) {
        root
            .findElement(datacenterLocation)
            .click();

        root
            .findElement(By.xpath("//div[contains(@id, 'select_container') and contains(@class, 'md-active')]//md-option[@value='" + value.getValue() + "']"))
            .click();

        return this;
    }

    public ComputeEngineTabComponent setCommittedUsage(CommittedUsage value) {
        root
            .findElement(committedUsage)
            .click();

        String index = String.valueOf(value.ordinal() + 1);

        root
            .findElement(By.xpath("//div[contains(@id, 'select_container') and contains(@class, 'md-active')]//md-option[" + index + "]"))
            .click();

        return this;
    }

    public ComputeEngineTabComponent clickAddToEstimate() {
        root
            .findElement(addToEstimateBtn)
            .click();

        return this;
    }
}
