package com.epam.training.student_julia_shtokolova;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class NewPasteTest {
    @Test
    public void newPasteTest() throws InterruptedException {
        WebDriver driver = new FirefoxDriver();
        driver.get("https://pastebin.com/");
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(20));


        // Filling out the text area for "New Paste"
        WebElement textArea = driver.findElement(By.xpath("//*[@id=\"postform-text\"]"));
        textArea.sendKeys("Hello from WebDriver");

        // Waiting for the banner
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("vi-smartbanner")));

        // Programmatically close the banner
        ((JavascriptExecutor) driver).executeScript("document.getElementById('vi-smartbanner').remove()");

        // Setting "Paste expiration"
        WebElement select = driver.findElement(By.id("select2-postform-expiration-container"));
        select.click();
        WebElement option = driver.findElement(By.xpath(
                "//*[@id='select2-postform-expiration-results']//li[@role='option' and text()='10 Minutes']"
        ));
        option.click();

        // Filling out "Paste name/Title
        WebElement pasteName = driver.findElement(By.id("postform-name"));
        pasteName.sendKeys("helloweb");

        // Submitting new paste
        WebElement createNewPasteBtn = driver.findElement(By.cssSelector(
                ".content .post-create form button[type=submit]"));
        createNewPasteBtn.click();

        driver.quit();
    }
}
